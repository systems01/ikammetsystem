// ignore: file_names
class DocumentItem {
  int? id;
  String? createdAt;
  String? updatedAt;
  String? filename;
  String? desc;
  String? clientId;

  DocumentItem(
    this.id,
    this.createdAt,
    this.updatedAt,
    this.filename,
    this.desc,
    this.clientId,
  );

  DocumentItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    filename = json['filename'];
    desc = json['desc'];
    clientId = json['client_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['filename'] = filename;
    data['desc'] = desc;
    data['client_id'] = clientId;
    return data;
  }
}
