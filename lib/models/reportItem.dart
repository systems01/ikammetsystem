class ReportItem {
  int? id;
  String? createdAt;
  String? updatedAt;
  String? filename;

  ReportItem(
    this.id,
    this.createdAt,
    this.updatedAt,
    this.filename,
  );

  ReportItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    filename = json['filename'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['filename'] = filename;
    return data;
  }

  getCreatedAtDate() {
    String date;
    if (createdAt!.contains("T")) {
      date = createdAt!.split("T")[0];
    } else {
      date = createdAt!.split(" ")[0];
    }
    return date;
  }

  getReportName() {
    String date;
    if (filename!.contains(".")) {
      date = filename!.split(".")[0];
    } else {
      date = filename!.split(" ")[0];
    }
    return date;
  }

  getUpdatedAtDate() {
    String date;
    if (updatedAt!.contains("T")) {
      date = updatedAt!.split("T")[0];
    } else {
      date = updatedAt!.split(" ")[0];
    }
    return date;
  }

  getCreatedAtTime() {
    String time;
    if (createdAt!.contains("T")) {
      time = createdAt!.split("T")[1].split(".")[0];
    } else {
      time = createdAt!.split("T")[1];
    }
    return time;
  }

  getUpdatedAtTime() {
    String time;
    if (updatedAt!.contains("T")) {
      time = updatedAt!.split("T")[1].split(".")[0];
    } else {
      time = updatedAt!.split("T")[1];
    }
    return time;
  }
}
