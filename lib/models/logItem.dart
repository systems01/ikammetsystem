class LogItem {
  int? id;
  String? logName;
  String? description;
  String? subjectType;
  String? subjectId;
  String? causerType;
  String? causerId;
  List properties;
  String? createdAt;
  String? updatedAt;

  LogItem(
      this.id,
      this.logName,
      this.description,
      this.subjectType,
      this.subjectId,
      this.causerType,
      this.causerId,
      this.properties,
      this.createdAt,
      this.updatedAt);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['log_name'] = logName;
    data['description'] = description;
    data['subject_type'] = subjectType;
    data['subject_id'] = subjectId;
    data['causer_type'] = causerType;
    data['causer_id'] = causerId;
    data['properties'] = properties.map((v) => v.toJson()).toList();
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }

  getCreatedAtDate() {
    String date;
    if (createdAt!.contains("T")) {
      date = createdAt!.split("T")[0];
    } else {
      date = createdAt!.split(" ")[0];
    }
    return date;
  }

  getUpdatedAtDate() {
    String date;
    if (updatedAt!.contains("T")) {
      date = updatedAt!.split("T")[0];
    } else {
      date = updatedAt!.split(" ")[0];
    }
    return date;
  }

  getCreatedAtTime() {
    String time;
    if (createdAt!.contains("T")) {
      time = createdAt!.split("T")[1].split(".")[0];
    } else {
      time = createdAt!.split("T")[1];
    }
    return time;
  }

  getUpdatedAtTime() {
    String time;
    if (updatedAt!.contains("T")) {
      time = updatedAt!.split("T")[1].split(".")[0];
    } else {
      time = updatedAt!.split("T")[1];
    }
    return time;
  }
}
