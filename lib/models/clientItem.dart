class ClientItem {
  String id;
  String firstName;
  String lastName;
  String mothersName;
  String dateOfExpiry;
  String startDate;
  String renewDate;
  String applicationStatus;
  String nationality;
  String birthDay;
  String kids;
  String isMarried;
  String idNumber;
  String phone;
  String passport;
  String createdAt;
  String updatedAt;
  List documents;

  ClientItem(
    this.id,
    this.firstName,
    this.lastName,
    this.mothersName,
    this.dateOfExpiry,
    this.startDate,
    this.renewDate,
    this.applicationStatus,
    this.nationality,
    this.birthDay,
    this.kids,
    this.isMarried,
    this.idNumber,
    this.phone,
    this.passport,
    this.createdAt,
    this.updatedAt,
    this.documents,
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['mothersName'] = mothersName;
    data['dateOfExpiry'] = dateOfExpiry;
    data['startDate'] = startDate;
    data['renewDate'] = renewDate;
    data['applicationStatus'] = applicationStatus;
    data['nationality'] = nationality;
    data['birthDay'] = birthDay;
    data['kids'] = kids;
    data['isMarried'] = isMarried;
    data['idNumber'] = idNumber;
    data['phone'] = phone;
    data['passport'] = passport;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['documents'] = documents.map((v) => v.toJson()).toList();
    return data;
  }
}
