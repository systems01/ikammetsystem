class UserItem {
  String? id;
  String? firstName;
  String? email;
  String? emailVerifiedAt;
  String? supperAdmin;
  String? createdAt;
  String? updatedAt;
  String? username;

  UserItem(
    this.id,
    this.firstName,
    this.email,
    this.emailVerifiedAt,
    this.supperAdmin,
    this.createdAt,
    this.updatedAt,
    this.username,
  );

  UserItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    supperAdmin = json['supperAdmin'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['firstName'] = firstName;
    data['email'] = email;
    data['email_verified_at'] = emailVerifiedAt;
    data['supperAdmin'] = supperAdmin;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['username'] = username;
    return data;
  }

  getCreatedAtDate() {
    String date;
    if (createdAt!.contains("T")) {
      date = createdAt!.split("T")[0];
    } else {
      date = createdAt!.split(" ")[0];
    }
    return date;
  }

  getUpdatedAtDate() {
    String date;
    if (updatedAt!.contains("T")) {
      date = updatedAt!.split("T")[0];
    } else {
      date = updatedAt!.split(" ")[0];
    }
    return date;
  }

  getCreatedAtTime() {
    String time;
    if (createdAt!.contains("T")) {
      time = createdAt!.split("T")[1].split(".")[0];
    } else {
      time = createdAt!.split("T")[1];
    }
    return time;
  }

  getUpdatedAtTime() {
    String time;
    if (updatedAt!.contains("T")) {
      time = updatedAt!.split("T")[1].split(".")[0];
    } else {
      time = updatedAt!.split("T")[1];
    }
    return time;
  }
}
