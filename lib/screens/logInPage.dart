import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/providers/logInProvider.dart';
import 'package:ikkametsystem/screens/landingPage.dart';
import 'package:ikkametsystem/screens/reSetPassword.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/LogInPage';
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LogInProvider newLoginProvider = LogInProvider();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool isObscureTextValue = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'تسجيل دخول',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 30, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(LandingPage.routeName);
            }),
      ),
      // endDrawer: AppDrawer(),
      body: Container(
        padding: const EdgeInsets.all(10),
        color: Theme.of(context).primaryColor,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            const Text(
              'الإسم',
              textAlign: TextAlign.right,
              style: TextStyle(
                  color: Colors.white, fontSize: 20, fontFamily: 'Jazeera'),
            ),
            TextFormField(
              controller: _usernameController,
              keyboardType: TextInputType.text,
              style: const TextStyle(fontSize: 18, color: Colors.black),
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(10),
                prefixIcon: Icon(
                  Icons.border_color,
                  color: Theme.of(context).primaryColor,
                ),
                hintText: 'إسم المُستخدم',
                filled: true,
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty ||
                    value.contains('@') ||
                    value.contains('123')) {
                  return 'من فضلك أدخل بيانات صحيحة';
                }
                return null;
              },
            ),
            const SizedBox(height: 10),
            const Text(
              'كلمة المرور',
              textAlign: TextAlign.right,
              style: TextStyle(
                  color: Colors.white, fontSize: 20, fontFamily: 'Jazeera'),
            ),
            TextFormField(
              controller: _passwordController,
              obscureText: isObscureTextValue,
              style: const TextStyle(fontSize: 18, color: Colors.black),
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                prefixIcon: isObscureTextValue
                    ? IconButton(
                        icon: Icon(
                          Icons.remove_red_eye_outlined,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: () {
                          setState(() {
                            isObscureTextValue = false;
                          });
                        },
                      )
                    : IconButton(
                        icon: Icon(
                          Icons.remove_red_eye,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: () {
                          setState(() {
                            isObscureTextValue = true;
                          });
                        },
                      ),
                contentPadding: const EdgeInsets.all(10),
                hintText: 'كلمة المرور',
                filled: true,
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty || value.length < 5) {
                  return 'كلمة المرور غير صحيحة';
                }
                return null;
              },
            ),
            const SizedBox(height: 10),
            GestureDetector(
              child: const Text(
                "نسيت كلمة المرور ؟",
                style: TextStyle(
                    fontFamily: 'Jazeera', fontSize: 20, color: Colors.white),
                textAlign: TextAlign.right,
              ),
              onTap: () async {
                Navigator.of(context)
                    .pushReplacementNamed(ReSetPassword.routeName);
              },
            ),
            const SizedBox(height: 10),
            // ignore: deprecated_member_use
            FlatButton(
              hoverColor: Colors.black45,
              child: const Text(
                "تسجيل دخول",
                style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
              ),
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: Colors.white, width: 2),
              ),
              padding: const EdgeInsets.all(15),
              textColor: Colors.white,
              onPressed: () async {
                var username = _usernameController.text;
                var password = _passwordController.text;
                if (username.length < 4) {
                  Fluttertoast.showToast(
                      msg: "لا يمكن إستخدام إسم اقل من 4 أحرف",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else if (password.length < 4) {
                  Fluttertoast.showToast(
                      msg: "لابد من إستخدام كلمة مرور أكبر من 4 رموز",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else {
                  newLoginProvider.login(username, password, context);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
