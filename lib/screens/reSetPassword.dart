import 'package:flutter/material.dart';
import 'package:ikkametsystem/screens/logInPage.dart';

class ReSetPassword extends StatefulWidget {
  static const routeName = '/reSetPassword';
  const ReSetPassword({Key? key}) : super(key: key);

  @override
  _ReSetPasswordState createState() => _ReSetPasswordState();
}

class _ReSetPasswordState extends State<ReSetPassword> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool isObscureTextValue = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'تغيير كلمة المرور',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 30, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(LoginPage.routeName);
            }),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        color: Theme.of(context).primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextFormField(
              controller: _usernameController,
              keyboardType: TextInputType.text,
              style: const TextStyle(fontSize: 18, color: Colors.black),
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.border_color,
                  color: Colors.red,
                ),
                contentPadding: const EdgeInsets.all(10),
                hintText: 'إسم المُستخدم',
                filled: true,
                fillColor: Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty ||
                    value.contains('@') ||
                    value.contains('123')) {
                  return 'من فضلك أدخل بيانات صحيحة';
                }
                return null;
              },
            ),
            const SizedBox(height: 10),
            TextFormField(
              controller: _passwordController,
              obscureText: isObscureTextValue,
              style: const TextStyle(fontSize: 18, color: Colors.black),
              textAlign: TextAlign.right,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(10),
                hintText: 'كلمة المرور',
                filled: true,
                fillColor: Colors.white,
                prefixIcon: isObscureTextValue
                    ? IconButton(
                        icon: Icon(
                          Icons.remove_red_eye_outlined,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: () {
                          setState(() {
                            isObscureTextValue = false;
                          });
                        },
                      )
                    : IconButton(
                        icon: Icon(
                          Icons.remove_red_eye,
                          color: Theme.of(context).primaryColor,
                        ),
                        onPressed: () {
                          setState(() {
                            isObscureTextValue = true;
                          });
                        },
                      ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty || value.length < 5) {
                  return 'كلمة المرور غير صحيحة';
                }
                return null;
              },
            ),
            const SizedBox(height: 10),
            // ignore: deprecated_member_use
            FlatButton(
              hoverColor: Colors.black45,
              child: const Text(
                "حفظ وإرسال",
                style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
              ),
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: Colors.white, width: 2),
              ),
              padding: const EdgeInsets.all(10),
              textColor: Colors.white,
              onPressed: () async {},
            ),
            const SizedBox(height: 10),
            GestureDetector(
              child: const Text(
                "لديك عضوية بالفعل ؟",
                style: TextStyle(
                    fontSize: 20, color: Colors.white, fontFamily: 'Jazeera'),
                textAlign: TextAlign.center,
              ),
              onTap: () {
                Navigator.of(context).pushReplacementNamed(LoginPage.routeName);
              },
            ),
          ],
        ),
      ),
    );
  }
}
