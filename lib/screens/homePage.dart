import 'package:flutter/material.dart';
import 'package:ikkametsystem/components/sideMenu.dart';
import 'package:ikkametsystem/controller/menuController.dart';
import 'package:ikkametsystem/dashboard/dashboardPage.dart';
import 'package:ikkametsystem/responsive/responsive.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

var name;

class HomePage extends StatefulWidget {
  static const routeName = '/HomePage';
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return _homePage(context);
  }
}

Widget _homePage(BuildContext context) {
  getUserNameValue();
  return Scaffold(
    key: context.read<MenuController>().scaffoldKey,
    drawer: const SideMenu(),
    appBar: AppBar(
      title: const Center(
        child: Text(
          'الصفحة العامة',
          textAlign: TextAlign.right,
          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
        ),
      ),
      automaticallyImplyLeading: false,
    ),
    body: SafeArea(
      child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Expanded(
          flex: 5,
          child: DashBoardPage(),
        ),
        if (Responsive.isDesktop(context))
          const Expanded(
            child: SideMenu(),
          ),
      ]),
    ),
  );
}

getUserNameValue() async {
  final prefs = await SharedPreferences.getInstance();
  name = prefs.getString('firstname').toString();
  return name;
}
