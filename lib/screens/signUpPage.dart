import 'package:flutter/material.dart';
import 'package:ikkametsystem/screens/landingPage.dart';
import 'package:ikkametsystem/screens/logInPage.dart';

class SignUpPage extends StatefulWidget {
  static const routeName = '/SignUpPage';
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final Map<String, String> _authData = {
    'fName': '',
    'lName': '',
    'email': '',
    'userName': '',
    'password': '',
  };

  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _obscureText = true;
  bool _obscureTextConfirm = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'تسجيل عضوية جديدة ',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(LandingPage.routeName);
            }),
      ),
      // endDrawer: AppDrawer(),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: LayoutBuilder(builder:
            (BuildContext context, BoxConstraints viewportConstraints) {
          return Container(
            padding: const EdgeInsets.all(10),
            color: Theme.of(context).primaryColor,
            width: double.infinity,
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: viewportConstraints.maxHeight,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      // const Icon(
                      //   Icons.person_add_alt,
                      //   size: 60,
                      //   color: Colors.white,
                      // ),
                      const Text(
                        'الإسم',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontFamily: 'Jazeera'),
                      ),
                      const SizedBox(height: 5),
                      TextFormField(
                          controller: _firstNameController,
                          keyboardType: TextInputType.text,
                          style: const TextStyle(
                              fontSize: 18, color: Colors.black),
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            hintText: 'الإسم الأول',
                            prefixIcon: const Icon(
                              Icons.border_color,
                              color: Colors.red,
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          validator: (value) {
                            if (value!.isEmpty ||
                                value.contains('@') ||
                                value.contains('123')) {
                              return 'من فضلك أدخل بيانات صحيحة';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _authData['fName'] = value!;
                          }),
                      const SizedBox(height: 5),
                      TextFormField(
                          controller: _lastNameController,
                          keyboardType: TextInputType.text,
                          style: const TextStyle(
                              fontSize: 18, color: Colors.black),
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            hintText: 'الإسم الثاني',
                            prefixIcon: const Icon(
                              Icons.border_color,
                              color: Colors.red,
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          validator: (value) {
                            if (value!.isEmpty ||
                                value.contains('@') ||
                                value.contains('123')) {
                              return 'من فضلك أدخل بيانات صحيحة';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _authData['lName'] = value!;
                          }),
                      const SizedBox(height: 5),
                      const Text(
                        'المعلومات الشخصية',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontFamily: 'Jazeera'),
                      ),
                      TextFormField(
                          controller: _emailController,
                          style: const TextStyle(
                              fontSize: 18, color: Colors.black),
                          textAlign: TextAlign.right,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty || !value.contains('@')) {
                              return 'بريد غير صحيح';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintText: 'البريد الإلكتروني',
                            prefixIcon: const Icon(
                              Icons.mail,
                              color: Colors.red,
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onSaved: (value) {
                            _authData['email'] = value!;
                          }),
                      const SizedBox(height: 5),
                      TextFormField(
                        controller: _userNameController,
                        keyboardType: TextInputType.text,
                        style:
                            const TextStyle(fontSize: 18, color: Colors.black),
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          hintText: 'إسم المُستخدم',
                          prefixIcon: const Icon(
                            Icons.border_color,
                            color: Colors.red,
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: const BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onSaved: (value) {
                          _authData['username'] = value!;
                        },
                        validator: (value) {
                          if (value!.isEmpty ||
                              value.contains('@') ||
                              value.contains('123')) {
                            return 'من فضلك أدخل بيانات صحيحة';
                          }
                          return null;
                        },
                      ),
                      const Text(
                        'كلمة المرور',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontFamily: 'Jazeera'),
                      ),
                      TextFormField(
                          obscureText: _obscureText,
                          controller: _passwordController,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 5) {
                              return 'كلمة مرور صغيرة ';
                            }
                            return null;
                          },
                          style: const TextStyle(
                              fontSize: 18, color: Colors.black),
                          textAlign: TextAlign.right,
                          decoration: InputDecoration(
                            prefixIcon: _obscureText
                                ? IconButton(
                                    icon: const Icon(
                                      Icons.remove_red_eye_outlined,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = false;
                                      });
                                    },
                                  )
                                : IconButton(
                                    icon: const Icon(
                                      Icons.remove_red_eye,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _obscureText = true;
                                      });
                                    },
                                  ),
                            contentPadding: const EdgeInsets.all(5),
                            hintText: 'كلمة المرور',
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: const BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          onSaved: (value) {
                            _authData['password'] = value!;
                          }),
                      const SizedBox(height: 5),
                      TextFormField(
                        obscureText: _obscureTextConfirm,
                        validator: (value) {
                          if (value != _passwordController.text) {
                            return 'كلمة مرور غير متطابقة';
                          }
                          return null;
                        },
                        style:
                            const TextStyle(fontSize: 18, color: Colors.black),
                        textAlign: TextAlign.right,
                        decoration: InputDecoration(
                          prefixIcon: _obscureTextConfirm
                              ? IconButton(
                                  icon: const Icon(
                                    Icons.remove_red_eye_outlined,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _obscureTextConfirm = false;
                                    });
                                  },
                                )
                              : IconButton(
                                  icon: const Icon(
                                    Icons.remove_red_eye,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _obscureTextConfirm = true;
                                    });
                                  },
                                ),
                          contentPadding: const EdgeInsets.all(5),
                          hintText: 'تأكيد كلمة المرور',
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: const BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      const SizedBox(height: 5),
                      // ignore: deprecated_member_use
                      FlatButton(
                        hoverColor: Colors.black45,
                        child: const Text(
                          " تسجيل عضوية جديدة",
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        ),
                        shape: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.white, width: 2),
                        ),
                        padding: const EdgeInsets.all(15),
                        textColor: Colors.white,
                        onPressed: () async {
                          // NewUser _newsUer = NewUser(
                          //     _firstNameController.text,
                          //     _lastNameController.text,
                          //     _userNameController.text,
                          //     _emailController.text,
                          //     _passwordController.text);
                          // bool resp =
                          //     await _signUpProvider.signUpUser(_newsUer);
                          // if (resp == true) {
                          //   Future.delayed(const Duration(milliseconds: 2500),
                          //       () async {
                          //     bool loginResp =
                          //         await _loginProvider.loginFirstTime(
                          //             _userNameController.text,
                          //             _passwordController.text);
                          //     if (loginResp == true) {
                          //       Future.delayed(
                          //           const Duration(milliseconds: 3500),
                          //           () async {
                          //         Fluttertoast.showToast(
                          //             msg:
                          //                 "لابد من الإشتراك في بعض المصادر حتى تظهر لك الأخبار اضغط على التصنيفات",
                          //             toastLength: Toast.LENGTH_SHORT,
                          //             gravity: ToastGravity.BOTTOM,
                          //             timeInSecForIosWeb: 3,
                          //             backgroundColor: Colors.green,
                          //             textColor: Colors.white,
                          //             fontSize: 16.0);
                          //       });
                          //       List checkCate = await _categoryProvider
                          //           .getCategorySubscribed();
                          //       if (checkCate.length == 0) {
                          //         Navigator.of(context).pushReplacementNamed(
                          //             ChoicesCategories.routName);
                          //       } else {
                          //         Navigator.of(context).pushReplacementNamed(
                          //             TabsScreen.routName);
                          //       }
                          //     }
                          //   });
                          // } else {}
                          // _submit();
                        },
                      ),
                      const SizedBox(height: 10),
                      GestureDetector(
                        child: const Text(
                          "لديك عضوية بالفعل ؟",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontFamily: 'Jazeera'),
                          textAlign: TextAlign.center,
                        ),
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed(LoginPage.routeName);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
