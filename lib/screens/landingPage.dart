import 'package:flutter/material.dart';
import 'package:ikkametsystem/screens/logInPage.dart';
import 'package:ikkametsystem/screens/signUpPage.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);
  static const routeName = '/LandingPage';
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).primaryColor,
        height: MediaQuery.of(context).size.width,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
                child: Image.network(
                    'https://c.tenor.com/4rjzFvNwsYwAAAAC/turkey-flag.gif'),
                height: 100,
                width: 100),
            const Text(
              '  مرحباً بك في نظام الإقامات التركية الخاص',
              style: TextStyle(
                  fontSize: 30, color: Colors.white, fontFamily: 'Jazeera'),
            ),
            const SizedBox(height: 20),
            // ignore: deprecated_member_use
            FlatButton(
              hoverColor: Colors.black45,
              child: const Text(
                "تسجيل دخول",
                style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
              ),
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: Colors.white, width: 2),
              ),
              padding: const EdgeInsets.all(15),
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(LoginPage.routeName);
              },
            ),
            const SizedBox(height: 10),
            // ignore: deprecated_member_use
            FlatButton(
              hoverColor: Colors.black45,
              child: const Text(
                "تسجيل عضوية جديدة",
                style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
              ),
              shape: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(color: Colors.white, width: 2),
              ),
              padding: const EdgeInsets.all(15),
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context)
                    .pushReplacementNamed(SignUpPage.routeName);
              },
            ),
            const SizedBox(height: 5),
          ],
        ),
      ),
    );
  }
}
