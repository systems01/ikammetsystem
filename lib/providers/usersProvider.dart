import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/models/userItem.dart';
import 'package:ikkametsystem/providers/sh_perfs.dart';
import '../models/userItem.dart';
import 'package:http/http.dart' as http;

var apiUrl = "http://api.openclues.com/api/";

var dio = Dio();
List<UserItem> users = [];

class UsersProvider {
  getAllUsersData() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'users',
      options: Options(headers: {
        "Content-Type": "application/json",
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.data}');
    var data = jsonDecode(response.data);
    print(data);
    return data;
  }

  getAllUsersDataHttp() async {
    String token = await ShPrefs.instance.getStringValue("token");

    var url = Uri.parse("http://api.openclues.com/api/users");
    var response = await http.get(url, headers: {
      "Content-Type": "application/json",
      "accept": "application/json",
      "Authorization": "Bearer $token",
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    var reponse = jsonDecode(response.body);
    return reponse;
  }

  addNewUser(String fName, String uName, String superAdmin, String password,
      String email) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.post(
      apiUrl +
          'users?firstName=$fName&username=$uName&supperAdmin=$superAdmin&password=$password&email=$email',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );

    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تمت الاضافة بنجاح",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "لم تتم الإضافة",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future<Map> getUserByID(String id) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'users/$id',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    print(response.data['user']);
    var result = response.data;
    print(result);
    return result;
  }

  Future<Map> getUserByIDHttp(String id) async {
    String token = await ShPrefs.instance.getStringValue("token");

    var url = Uri.parse("http://api.openclues.com/api/users/$id");
    var response = await http.get(url, headers: {
      "Content-Type": "application/json",
      "accept": "application/json",
      "Authorization": "Bearer $token",
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    var reponse = jsonDecode(response.body);
    return reponse;
  }

  deleteUserByID(String id) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.delete(
      apiUrl + 'users/$id',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تم بنجاح",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "لم يتم",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  updateUserData(String id, String fName, String uName, String superAdmin,
      String password, String email) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.put(
      apiUrl +
          'users?id=$id&firstName=$fName&username=$uName&supperAdmin=$superAdmin&password=$password&email=$email',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );

    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تمت الاضافة بنجاح",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      print(response.statusCode);
      Fluttertoast.showToast(
          msg: "لم تتم الإضافة",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  getUsersCount() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'usersCount',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (response.statusCode == 200) {
      if (kDebugMode) {
        print(response.data["Users count"]);
      }
      return response.data["Users count"];
    } else {
      if (kDebugMode) {
        print(response.statusCode);
      }
    }
    var data = response.data;
    return data;
  }

  Future<Map> getUserByNameOrMail(String parm) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'user/search/$parm',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    print(response.statusCode);
    print(response.data);
    var result = response.data;
    print(result);
    return result;
  }

  Future<Map> getUserByNameOrMailHttp(String parm) async {
    String token = await ShPrefs.instance.getStringValue("token");

    var url = Uri.parse("http://api.openclues.com/api/user/search/$parm");
    var response = await http.get(url, headers: {
      "Content-Type": "application/json",
      "accept": "application/json",
      "Authorization": "Bearer $token",
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    var reponse = jsonDecode(response.body);
    return reponse;
  }
}
