import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:ikkametsystem/models/documentItem.dart';
import 'package:ikkametsystem/providers/sh_perfs.dart';

var apiUrl = "http://api.openclues.com/api/";

var dio = Dio();
List<DocumentItem> documents = [];

class DocumentsProvider {
  Future<List<DocumentItem>> getAllDocumentsByClientsID(String clientID) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'clients/documents/$clientID',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
    for (var i in response.data) {
      DocumentItem _item = DocumentItem(
        i["id"],
        i["created_at"],
        i["updated_at"],
        i["filename"],
        i["decs"],
        i["client_id"],
      );
      documents.add(_item);
    }
    if (kDebugMode) {
      print("Here Look");
    }
    if (kDebugMode) {
      print(documents);
    }
    return documents;
  }

  Future<List<DocumentItem>> addDocumentsByClientsID(String clientID) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.post(
      apiUrl + 'clients/documents/$clientID',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
    return documents;
  }

  deleteDocumentsID(String documentID) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.delete(
      apiUrl + 'documents/$documentID',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
  }

  Future<List<DocumentItem>> getDocumentByID(String documentID) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'clients/documents/$documentID',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
    for (var i in response.data) {
      DocumentItem _item = DocumentItem(
        i["id"],
        i["created_at"],
        i["updated_at"],
        i["filename"],
        i["decs"],
        i["client_id"],
      );
      documents.add(_item);
    }
    if (kDebugMode) {
      print("Here Look");
    }
    if (kDebugMode) {
      print(documents);
    }
    return documents;
  }
}
