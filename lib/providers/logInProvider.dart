import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/providers/sh_perfs.dart';

var apiUrl = "http://api.openclues.com/api/";

var dio = Dio();
var errorCode = '';

class LogInProvider {
  login(String username, var password, context) async {
    // final prefs = await SharedPreferences.getInstance();
    try {
      Response response = await dio.post(
          apiUrl + 'user/login?username=$username&password=$password',
          data: {
            "username": username,
            "password": password,
          },
          options: Options(headers: {
            "accept": "application/json",
          }));
      print(response.data['token'].toString());
      print(response.data['user']['firstName'].toString());
      print(response.statusCode);
      if (response.statusCode == 200) {
        Navigator.pushNamed(context, '/HomePage');
        ShPrefs.instance
            .setStringValue('email', response.data['user']["email"].toString());
        ShPrefs.instance
            .setStringValue('id', response.data['user']["id"].toString());
        ShPrefs.instance.setStringValue(
            'firstname', response.data['user']["firstName"].toString());
        ShPrefs.instance.setStringValue(
            'supperAdmin', response.data['user']["supperAdmin"].toString());
        ShPrefs.instance.setStringValue(
            'created_at', response.data['user']["created_at"].toString());
        ShPrefs.instance.setStringValue(
            'updated_at', response.data['user']["updated_at"].toString());
        Fluttertoast.showToast(
          msg: "تم التسجيل مرحباً بك",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0,
        );
        //if request success
        if (response.data['token'] != null) {
          await ShPrefs.instance
              .setStringValue('token', response.data['token'].toString());
        }
        return true;
      }
    } on DioError catch (e) {
      print('login error ' + e.toString());
      print(e.response?.statusCode);
      if (e.response?.statusCode == 400) {
        //if API not found
        Fluttertoast.showToast(
            msg: "الخدمة غير متاحة حاليا حاول لاحقا",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      if (e.response?.statusCode == 401) {
        Fluttertoast.showToast(
            msg: "من فضلك تأكد من إسم المستخدم و كلمة المرور",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      if (e.response?.statusCode == 403) {
        Fluttertoast.showToast(
            msg: '${e.response}',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      if (e.response?.statusCode == 422) {
        Fluttertoast.showToast(
            msg: "Invalid token/header",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      if (e.response?.statusCode == 503) {
        Fluttertoast.showToast(
            msg: "من فضلك تأكد من صحة البيانات",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      return false;
    }
    return false;
  }
}
