import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/models/logItem.dart';
import 'package:ikkametsystem/providers/sh_perfs.dart';

var dio = Dio();
List<LogItem> logs = [];
var apiUrl = "http://api.openclues.com/api/";

class LogsProvider {
  Future<List<LogItem>> getAllLogs() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'logs',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    for (var i in response.data["logs"]) {
      LogItem _item = LogItem(
        i["id"],
        i["log_name"],
        i["description"],
        i["subject_type"],
        i["subject_id"],
        i["causer_type"],
        i["causer_id"],
        i["properties"],
        i["created_at"],
        i["updated_at"],
      );
      logs.add(_item);
    }
    return logs;
  }

  Future<List<LogItem>> getLogsByID(String iD) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'logs/$iD',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    print(apiUrl + 'logs/$iD');
    print(response.data);
    for (var i in response.data["logs"]) {
      LogItem _item = LogItem(
        i["id"],
        i["log_name"],
        i["description"],
        i["subject_type"],
        i["subject_id"],
        i["causer_type"],
        i["causer_id"],
        i["properties"],
        i["created_at"],
        i["updated_at"],
      );
      logs.add(_item);
    }
    return logs;
  }

  deleteLogsByID(String iD) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.delete(
      apiUrl + 'logs/$iD',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    print(apiUrl + 'logs/$iD');
    print(response.data);
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
        msg: "تم الحذف بنجاح",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0,
      );
    }
  }
}
