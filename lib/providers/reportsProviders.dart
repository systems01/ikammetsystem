import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/models/reportItem.dart';
import 'package:ikkametsystem/providers/sh_perfs.dart';

var apiUrl = "http://api.openclues.com/api/";
var dio = Dio();
List<ReportItem> reports = [];

class ReportsProvider {
  Future<List<ReportItem>> getAllReportsDataNew() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'reports',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    for (var i in response.data) {
      ReportItem _item = ReportItem(
        i["id"],
        i["created_at"],
        i["updated_at"],
        i["filename"],
      );
      reports.add(_item);
    }
    return reports;
  }

  Future<List<ReportItem>> getReportDataByID(String id) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'reports/$id',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    for (var i in response.data) {
      ReportItem _item = ReportItem(
        i["id"],
        i["created_at"],
        i["updated_at"],
        i["filename"],
      );
      reports.add(_item);
    }
    return reports;
  }

  generateReportByAllFields() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'reports/generate',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تمت اصدار ملف بنجاح",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      if (kDebugMode) {
        print(response.statusCode);
      }
    }
  }

  Future<List<ReportItem>> getReportDataByAny(String parm) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'reports/generate/$parm',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    for (var i in response.data) {
      ReportItem _item = ReportItem(
        i["id"],
        i["created_at"],
        i["updated_at"],
        i["filename"],
      );
      reports.add(_item);
    }
    return reports;
  }
}
