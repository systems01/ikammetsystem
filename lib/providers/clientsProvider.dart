import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/models/clientItem.dart';
import 'package:ikkametsystem/providers/sh_perfs.dart';
import 'package:http/http.dart' as http;

var apiUrl = "http://api.openclues.com/api/";

var dio = Dio();
List<ClientItem> clients = [];

class ClientsProvider {
  Future<List<ClientItem>> getAllClientsData() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'clients',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
    for (var i in response.data) {
      ClientItem _item = ClientItem(
        i["id"],
        i["firstName"],
        i["lastName"],
        i["mothersName"],
        i["dateOfExpiry"],
        i["startDate"],
        i["renewDate"],
        i["applicationStatus"],
        i["nationality"],
        i["birthDay"],
        i["kids"],
        i["isMarried"],
        i["idNumber"],
        i["phone"],
        i["passport"],
        i["created_at"],
        i["updated_at"],
        i["documents"],
      );
      clients.add(_item);
    }
    if (kDebugMode) {
      print("Here Look");
    }
    if (kDebugMode) {
      print(clients);
    }
    return clients;
  }

  getAllClientsDataHttps() async {
    String token = await ShPrefs.instance.getStringValue("token");

    var url = Uri.parse("http://api.openclues.com/api/clients");

    var response = await http.get(url, headers: {
      "Content-Type": "application/json",
      "accept": "application/json",
      "Authorization": "Bearer $token",
    });

    var reponse = jsonDecode(response.body);
    return reponse;
  }

  addNewClientData(
      String firstName,
      String passport,
      String idNumber,
      String isMarried,
      String birthDay,
      String renewDate,
      String startDate,
      String mothersName,
      String lastName,
      String phone,
      String dateOfExpiry,
      String nationality,
      String applicationStatus,
      String kids,
      String createdAt,
      String updatedAt,
      List documents) async {
    String token = await ShPrefs.instance.getStringValue("token");
    print(apiUrl +
        'clients?firstName=$firstName&passport=$passport&idNumber=$idNumber&isMarried=$isMarried&birthDay=$birthDay&renewDate=$renewDate&startDate=$startDate&mothersName=$mothersName&lastName=$lastName&phone=$phone&dateOfExpiry=$dateOfExpiry&nationality=$nationality&applicationStatus=$applicationStatus&kids=$kids&createdAt=$createdAt&updatedAt=$updatedAt&documents=$documents');
    Response response = await dio.post(
      apiUrl +
          'clients?firstName=$firstName&passport=$passport&idNumber=$idNumber&isMarried=$isMarried&birthDay=${Uri.encodeFull(birthDay)}&renewDate=${Uri.encodeFull(renewDate)}&startDate=${Uri.encodeFull(startDate)}&mothersName=$mothersName&lastName=$lastName&phone=$phone&dateOfExpiry=${Uri.encodeFull(dateOfExpiry)}&nationality=$nationality&applicationStatus=$applicationStatus&kids=$kids&createdAt=${Uri.encodeFull(createdAt)}&updatedAt=${Uri.encodeFull(updatedAt)}&documents=$documents',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تمت الاضافة بنجاح",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "لم تتم الإضافة",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  Future<Map> getClientDataBYID(String id) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'clients/$id',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    return response.data;
  }

  getClientDataExpiredBYDaysNum(int expiredDays) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + 'clients/expired/$expiredDays',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    return response.data;
  }

  updateClientData(
      String id,
      String firstName,
      String passport,
      String idNumber,
      String isMarried,
      String birthDay,
      String renewDate,
      String startDate,
      String mothersName,
      String lastName,
      String phone,
      String dateOfExpiry,
      String nationality,
      String applicationStatus,
      String kids,
      String createdAt,
      String updatedAt,
      List documents) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.put(
      apiUrl +
          'clients/$id?firstName=$firstName&passport=$passport&idNumber=$idNumber&isMarried=$isMarried&birthDay=$birthDay&renewDate=$renewDate&startDate=$startDate&mothersName=$mothersName&lastName=$lastName&phone=$phone&dateOfExpiry=$dateOfExpiry&nationality$nationality&applicationStatus=$applicationStatus&kids=$kids&createdAt=$createdAt&updatedAt=$updatedAt&documents=$documents',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (response.statusCode == 200) {
      Fluttertoast.showToast(
          msg: "تمت الاضافة بنجاح",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      Fluttertoast.showToast(
          msg: "لم تتم الإضافة",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  deleteClientDataBYID(String id) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.delete(
      apiUrl + 'clients/$id',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
  }

  getClientsCount() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.delete(
      apiUrl + 'clientsCount',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
    return response.data;
  }

  Future<List<ClientItem>> searchClientDataBYParm(String parm) async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.get(
      apiUrl + '/client/search/$parm',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
    for (var i in response.data) {
      ClientItem _item = ClientItem(
        i["id"],
        i["firstName"],
        i["lastName"],
        i["mothersName"],
        i["dateOfExpiry"],
        i["startDate"],
        i["renewDate"],
        i["applicationStatus"],
        i["nationality"],
        i["birthDay"],
        i["kids"],
        i["isMarried"],
        i["idNumber"],
        i["phone"],
        i["passport"],
        i["created_at"],
        i["updated_at"],
        i["documents"],
      );
      clients.add(_item);
    }
    if (kDebugMode) {
      print("Here Look");
    }
    if (kDebugMode) {
      print(clients);
    }
    return clients;
  }

  deleteAllClientsData() async {
    String token = await ShPrefs.instance.getStringValue("token");
    Response response = await dio.delete(
      apiUrl + 'clients',
      options: Options(headers: {
        "accept": "application/json",
        "Authorization": "Bearer $token",
      }),
    );
    if (kDebugMode) {
      print(response.data);
    }
  }
}
