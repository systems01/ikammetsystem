import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ikkametsystem/clientsPages/ClientsPage.dart';
import 'package:ikkametsystem/models/clientItem.dart';
import 'package:ikkametsystem/providers/clientsProvider.dart';

ClientsProvider _clientsProvider = ClientsProvider();
TextEditingController searchController = TextEditingController();

class ClientsListData extends StatelessWidget {
  const ClientsListData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        controller: ScrollController(),
        child: Column(
          children: <Widget>[
            Row(children: [
              const Expanded(
                flex: 3,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: SearchClientsField(),
                ),
              ),
              clientsOptions(context),
            ]),
            Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              height: MediaQuery.of(context).size.height - 80,
              child: FutureBuilder(
                future: _clientsProvider.getAllClientsDataHttps(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (kDebugMode) {
                    print(snapshot.data);
                  }
                  if (snapshot.data == null ||
                      snapshot.data == ConnectionState.none ||
                      snapshot.data == ConnectionState.waiting) {
                    return const Center(
                      child: Text('من فضلك إنتظر '),
                    );
                  } else {
                    return ListView.builder(
                      controller: ScrollController(),
                      itemCount: snapshot.data.length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      addAutomaticKeepAlives: false,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () async {
                            // Navigator.of(context)
                            //     .pushReplacementNamed(UserDataDetails.routName);
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //     builder: (context) => ClientDataDetails(snapshot
                            //         .data!['users'][index]['id']
                            //         .toString()),
                            //   ),
                            // );
                          },
                          child: ClipRRect(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Card(
                                color: Colors.green,
                                margin: const EdgeInsets.only(
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 10.0,
                                    top: 0.0),
                                elevation: 4.0,
                                child: Column(
                                  children: [
                                    Row(
                                      children: <Widget>[
                                        Stack(
                                          children: const <Widget>[
                                            Icon(
                                              Icons.person,
                                              size: 50,
                                              color: Colors.white,
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        right: 8.0,
                                                      ),
                                                      child: Text(
                                                        ' ${snapshot.data[index]['id'].toString()}  :  رقم العميل  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["firstName"]}  :  إسم العميل  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["lastName"]}  :  اللقب  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["phone"]}  :  رقم الهاتف  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["nationality"]}  :  الجنسية  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["idNumber"]}  :  رقم الإقامة  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget clientsOptions(BuildContext context) {
  return Container(
    color: Colors.transparent,
    padding: const EdgeInsets.all(10),
    child: Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FloatingActionButton.extended(
            heroTag: "btn3",
            backgroundColor: Colors.green,
            hoverColor: Colors.transparent,
            onPressed: (() {
              Navigator.of(context).pushReplacementNamed("/AddNewClient");
            }),
            label: const Text(
              'أضف عميل جديد',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontSize: 15, fontFamily: 'Jazeera', color: Colors.white),
            ),
            icon: const Icon(Icons.add),
          ),
        ],
      ),
    ]),
  );
}

class SearchClientsField extends StatelessWidget {
  const SearchClientsField();
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: searchController,
      textAlign: TextAlign.right,
      decoration: InputDecoration(
        hintText: " بحث في العملاء",
        hintStyle: const TextStyle(
          fontSize: 10,
          fontFamily: 'Jazeera',
        ),
        fillColor: Colors.black12,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        suffixIcon: InkWell(
          onTap: () {
            _clientsProvider.getClientDataExpiredBYDaysNum(30);
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //     builder: (context) => UserSearch(searchController.text),
            //   ),
            // );
          },
          child: Container(
            margin: const EdgeInsets.all(10 / 2),
            padding: const EdgeInsets.all(20 * 0.75),
            //   decoration: const BoxDecoration(color: Colors.blue),
            child: const Icon(Icons.search),
          ),
        ),
      ),
    );
  }
}
