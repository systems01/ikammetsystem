import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/components/countyList.dart';
import 'package:ikkametsystem/components/sideMenu.dart';
import 'package:ikkametsystem/clientsPages/ClientsPage.dart';
import 'package:ikkametsystem/providers/clientsProvider.dart';
import 'package:select_form_field/select_form_field.dart';

class AddNewClient extends StatefulWidget {
  static const routeName = '/AddNewClient';
  const AddNewClient({Key? key}) : super(key: key);

  @override
  _AddNewClientState createState() => _AddNewClientState();
}

class _AddNewClientState extends State<AddNewClient> {
  final TextEditingController _fNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _motherNameController = TextEditingController();
  final TextEditingController _fileStatusController = TextEditingController();
  final TextEditingController _marraigeStatusController =
      TextEditingController();
  final TextEditingController _nationalityNameController =
      TextEditingController();
  final TextEditingController _birthdayController = TextEditingController();
  final TextEditingController _childrenNumbersController =
      TextEditingController();
  final TextEditingController _startDateResController = TextEditingController();
  final TextEditingController _renewDateResController = TextEditingController();
  final TextEditingController _endDateResController = TextEditingController();
  final TextEditingController _resNumberController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _passportNumberController =
      TextEditingController();
  String imageCat = 'assets/images/13.png';
  final ClientsProvider _clientsProvider = ClientsProvider();
  final List<Map<String, dynamic>> _marraigeItems = [
    {
      'value': 'Married',
      'label': 'متزوج',
      'icon': const Icon(Icons.supervised_user_circle_sharp),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'Single',
      'label': 'أعزب',
      'icon': const Icon(Icons.person_pin_outlined),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'Divorced',
      'label': 'مطلق',
      'icon': const Icon(Icons.accessibility_rounded),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
  ];

  final List<Map<String, dynamic>> _fileStatusItems = [
    {
      'value': 'FirstTimeResidence',
      'label': 'تقديم أول مرة',
      'icon': const Icon(Icons.filter_1_rounded),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'FirstTimeRenewResidence',
      'label': 'تجديد لأول مرة',
      'icon': const Icon(Icons.filter_vintage_outlined),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'PublicRenewResidence',
      'label': 'تجديد',
      'icon': const Icon(Icons.update),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'HumanityResidence',
      'label': 'إقامة إنسانية',
      'icon': const Icon(Icons.person_pin),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'LongTermResidence',
      'label': 'إقامة دائمة',
      'icon': const Icon(Icons.receipt_long),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'ExceptionResidence',
      'label': 'إقامة إستثنائية',
      'icon': const Icon(Icons.expand),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'RealEstateResidence',
      'label': 'إقامة عقارية',
      'icon': const Icon(Icons.home),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': 'FamilyResidence',
      'label': 'إقامة عائلية',
      'icon': const Icon(Icons.family_restroom),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'إضافة عميل جديد',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(ClientsPage.routeName);
            }),
      ),
      body: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Expanded(
          flex: 5,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Image.asset(
                        imageCat,
                        fit: BoxFit.fill,
                        height: 150,
                      ),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'الإسم الأول',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'اللقب',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            controller: _fNameController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل الاسم الأول',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            controller: _lastNameController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل إسم اللقب',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'الإسم الأم',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'الحالة الإجتماعية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            controller: _motherNameController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل الاسم الأم',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: SelectFormField(
                            controller: _marraigeStatusController,
                            type: SelectFormFieldType
                                .dropdown, // or can be dialog
                            //initialValue: '1',
                            icon: const Icon(Icons.dashboard_outlined),
                            labelText: 'إختر الحالة الإجتماعية',
                            style: const TextStyle(
                                fontSize: 10, fontFamily: 'Jazeera'),
                            items: _marraigeItems,
                            onChanged: (val) => print(val),
                            onSaved: (val) =>
                                _marraigeStatusController.text = val!,
                          ),
                        )
                      ]),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'حالة الملف',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'الجنسية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: SelectFormField(
                            controller: _fileStatusController,
                            type: SelectFormFieldType
                                .dropdown, // or can be dialog
                            //initialValue: '1',
                            icon: const Icon(Icons.zoom_out_map),
                            labelText: 'إختر حالة الملف',
                            style: const TextStyle(
                                fontSize: 10, fontFamily: 'Jazeera'),
                            items: _fileStatusItems,
                            onChanged: (val) => print(val),
                            onSaved: (val) => _fileStatusController.text = val!,
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: SelectFormField(
                            controller: _marraigeStatusController,
                            type: SelectFormFieldType
                                .dropdown, // or can be dialog
                            //initialValue: '1',
                            icon: const Icon(Icons.place),
                            labelText: 'إختر الجنسية للعميل',
                            style: const TextStyle(
                                fontSize: 10, fontFamily: 'Jazeera'),
                            items: list,
                            onChanged: (val) => print(val),
                            onSaved: (val) =>
                                _marraigeStatusController.text = val!,
                          ),
                        )
                      ]),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'تاريخ الميلاد',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'عدد الأولاد',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            onTap: () {
                              _selectBirthDay();
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            controller: _birthdayController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل تاريخ الميلاد',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            controller: _childrenNumbersController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل عدد الأولاد',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'تاريخ بدء الإقامة',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'تاريخ تجديد الإقامة',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            onTap: () {
                              _selectStartDate();
                            },
                            controller: _startDateResController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل تاريخ بدء الإقامة ',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            onTap: () {
                              _selectReNewDate();
                            },
                            controller: _renewDateResController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل تاريخ تجديد الاقامة',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'تاريخ إنتهاء الإقامة',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'رقم الإقامة',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            onTap: () {
                              _selectEndDate();
                            },
                            controller: _endDateResController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل تاريخ إنتهاء الاقامة',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            controller: _resNumberController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل رقم الإقامة الحالي',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'رقم الهاتف',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'رقم جواز السفر',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            controller: _phoneNumberController,
                            decoration: const InputDecoration(
                              hintText: '(+90 555 555 1234)',
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل رقم الهاتف',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            controller: _passportNumberController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              hintText: '(A123457)',
                              labelText: 'أدخل رقم جواز السفر',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      const SizedBox(height: 30),
                      Row(
                        children: [
                          Expanded(
                            child: MaterialButton(
                              onPressed: () async {
                                if (kDebugMode) {
                                  print(_marraigeStatusController.text);
                                }
                                if (_fNameController.text.isEmpty ||
                                    _lastNameController.text.isEmpty) {
                                  Fluttertoast.showToast(
                                      msg: "لا يمكن إضافة بإسم فارغ",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                } else {
                                  _clientsProvider.addNewClientData(
                                      _fNameController.text,
                                      _passportNumberController.text,
                                      _resNumberController.text,
                                      _marraigeStatusController.text,
                                      _birthdayController.text,
                                      _renewDateResController.text,
                                      _startDateResController.text,
                                      _motherNameController.text,
                                      _lastNameController.text,
                                      _phoneNumberController.text,
                                      _endDateResController.text,
                                      _nationalityNameController.text,
                                      _fileStatusController.text,
                                      _childrenNumbersController.text,
                                      _startDateResController.text,
                                      _renewDateResController.text, []);
                                }
                              },
                              child: const Text(
                                "إضافة عميل جديد ",
                                style: TextStyle(
                                  fontFamily: 'Jazeera',
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                              color: Colors.green,
                            ),
                          ),
                          const SizedBox(width: 3),
                          Expanded(
                            child: MaterialButton(
                              onPressed: () {
                                clearTextController();
                              },
                              child: const Text(
                                "مسح جميع البيانات",
                                style: TextStyle(
                                  fontFamily: 'Jazeera',
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                              color: Colors.blueAccent,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        const Expanded(
          child: SideMenu(),
        ),
      ]),
    );
  }

  clearTextController() {
    _fNameController.clear();
    _phoneNumberController.clear();
    _lastNameController.clear();
    _passportNumberController.clear();
    _motherNameController.clear();
    _resNumberController.clear();
    _renewDateResController.clear();
    _endDateResController.clear();
    _startDateResController.clear();
    _nationalityNameController.clear();
    _birthdayController.clear();
    _childrenNumbersController.clear();
    _fileStatusController.clear();
    _marraigeStatusController.clear();
  }

  Future _selectBirthDay() async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2022),
        lastDate: DateTime(2030));
    if (picked != null) {
      setState(() => _birthdayController.text = picked.toString());
    }
  }

  Future _selectStartDate() async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2022),
        lastDate: DateTime(2030));
    if (picked != null) {
      setState(() => _startDateResController.text = picked.toString());
    }
  }

  Future _selectEndDate() async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2022),
        lastDate: DateTime(2030));
    if (picked != null) {
      setState(() => _endDateResController.text = picked.toString());
    }
  }

  Future _selectReNewDate() async {
    DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2022),
        lastDate: DateTime(2030));
    if (picked != null) {
      setState(() => _renewDateResController.text = picked.toString());
    }
  }
}
