import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../providers/clientsProvider.dart';

ClientsProvider _clientsProvider = ClientsProvider();

class ClientsNeedUpdateList extends StatelessWidget {
  const ClientsNeedUpdateList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        controller: ScrollController(),
        child: Column(
          children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              //height: 380,
              child: FutureBuilder(
                future: _clientsProvider.getClientDataExpiredBYDaysNum(30),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (kDebugMode) {
                    print(snapshot.data);
                  }
                  if (snapshot.data == null ||
                      snapshot.data == ConnectionState.none ||
                      snapshot.data == ConnectionState.waiting) {
                    return const Center(
                      child: Text('من فضلك إنتظر '),
                    );
                  } else {
                    return ListView.builder(
                      reverse: true,
                      controller: ScrollController(),
                      itemCount: snapshot.data.length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      addAutomaticKeepAlives: false,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () async {
                            // Navigator.of(context)
                            //     .pushReplacementNamed(UserDataDetails.routName);
                            // Navigator.push(
                            //   context,
                            //   MaterialPageRoute(
                            //     builder: (context) => ClientDataDetails(snapshot
                            //         .data!['users'][index]['id']
                            //         .toString()),
                            //   ),
                            // );
                          },
                          child: ClipRRect(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Card(
                                color: Colors.green,
                                margin: const EdgeInsets.only(
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 10.0,
                                    top: 0.0),
                                elevation: 4.0,
                                child: Column(
                                  children: [
                                    Row(
                                      children: <Widget>[
                                        Stack(
                                          children: const <Widget>[
                                            Icon(
                                              Icons.person,
                                              size: 50,
                                              color: Colors.white,
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        right: 8.0,
                                                      ),
                                                      child: Text(
                                                        ' ${snapshot.data[index]['id'].toString()}  :  رقم العميل  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["firstName"]}  :  إسم العميل  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["lastName"]}  :  اللقب  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["phone"]}  :  رقم الهاتف  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["nationality"]}  :  الجنسية  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data[index]["idNumber"]}  :  رقم الإقامة  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
