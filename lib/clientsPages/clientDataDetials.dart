import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/models/userItem.dart';
import 'package:ikkametsystem/providers/usersProvider.dart';
import 'package:ikkametsystem/usersPages/usersPage.dart';
import 'package:provider/provider.dart';
import 'package:select_form_field/select_form_field.dart';
import '../components/sideMenu.dart';
import '../controller/menuController.dart';
import '../responsive/responsive.dart';

String imageCat = 'assets/images/14.png';
UsersProvider _usersProvider = UsersProvider();

var userName;
var userEmail;

class ClientDataDetails extends StatefulWidget {
  static const routName = '/UserDataDetails';
  final String _userItemID;
  const ClientDataDetails(this._userItemID, {Key? key}) : super(key: key);

  @override
  State<ClientDataDetails> createState() => _ClientDataDetailsState();
}

class _ClientDataDetailsState extends State<ClientDataDetails> {
  final TextEditingController _fNameController =
      TextEditingController(text: userName);
  final TextEditingController _uNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController =
      TextEditingController(text: userEmail);
  final TextEditingController _superAdminController = TextEditingController();
  final List<Map<String, dynamic>> _items = [
    {
      'value': '1',
      'label': 'أدمن',
      'icon': const Icon(Icons.stop),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': '0',
      'label': 'مستخدم عادي',
      'icon': const Icon(Icons.fiber_manual_record),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: context.read<MenuController>().scaffoldKey,
      drawer: const SideMenu(),
      appBar: AppBar(
        title: Center(
          child: Text(
            ' ${widget._userItemID} بيانات العميل   ',
            textAlign: TextAlign.right,
            style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(UsersPage.routeName);
            }),
      ),
      body: SafeArea(
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(children: [
                Image.asset(
                  imageCat,
                  fit: BoxFit.fill,
                  height: 230,
                ),
                FutureBuilder(
                  future: _usersProvider.getUserByID(widget._userItemID),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.data == null ||
                        snapshot.connectionState == ConnectionState.waiting ||
                        snapshot.data == ConnectionState.none) {
                      return const Center(
                        child: Text('من فضلك إنتظر '),
                      );
                    } else {
                      return SingleChildScrollView(
                        child: Directionality(
                          textDirection: TextDirection.rtl,
                          child: Column(
                            children: [
                              Row(children: const [
                                Expanded(
                                    child: Text(
                                  'الإسم',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 18, fontFamily: 'Jazeera'),
                                )),
                                Spacer(),
                                Expanded(
                                    child: Text(
                                  'إسم العميل',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 20, fontFamily: 'Jazeera'),
                                )),
                              ]),
                              Row(children: [
                                Expanded(
                                  child: TextFormField(
                                    initialValue: snapshot.data["user"]
                                        ["firstName"],
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      labelText: 'أدخل الاسم',
                                      hintText: snapshot.data["user"]
                                          ["firstName"],
                                      labelStyle: const TextStyle(
                                          fontSize: 10, fontFamily: 'Jazeera'),
                                    ),
                                  ),
                                ),
                                const Spacer(),
                                Expanded(
                                  child: TextFormField(
                                    initialValue: snapshot.data["user"]
                                        ["username"],
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      hintText: snapshot.data["user"]
                                          ["username"],
                                      labelText: 'أدخل إسم العميل',
                                      labelStyle: const TextStyle(
                                          fontSize: 10, fontFamily: 'Jazeera'),
                                    ),
                                  ),
                                ),
                              ]),
                              const SizedBox(height: 20),
                              Row(children: const [
                                Expanded(
                                    child: Text(
                                  'البريد',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 20, fontFamily: 'Jazeera'),
                                )),
                                Spacer(),
                                Expanded(
                                    child: Text(
                                  'نوع الحساب ',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 18, fontFamily: 'Jazeera'),
                                )),
                              ]),
                              Row(children: [
                                Expanded(
                                  child: TextFormField(
                                    initialValue: snapshot.data["user"]
                                        ["email"],
                                    decoration: InputDecoration(
                                      border: const UnderlineInputBorder(),
                                      labelText: 'أدخل البريد الإلكتروني',
                                      hintText: snapshot.data["user"]["email"],
                                      labelStyle: const TextStyle(
                                          fontSize: 10, fontFamily: 'Jazeera'),
                                    ),
                                  ),
                                ),
                                const Spacer(),
                                Expanded(
                                  child: SelectFormField(
                                    initialValue: snapshot.data["user"]
                                        ["supperAdmin"],
                                    type: SelectFormFieldType
                                        .dropdown, // or can be dialog
                                    //initialValue: '1',
                                    icon: const Icon(Icons.format_shapes),
                                    labelText: 'إختر نوع الحساب',
                                    style: const TextStyle(
                                        fontSize: 10, fontFamily: 'Jazeera'),
                                    items: _items,
                                    onChanged: (val) => print(val),
                                    onSaved: (val) =>
                                        _superAdminController.text = val!,
                                  ),
                                )
                              ]),
                              const SizedBox(height: 30),
                              Row(
                                children: [
                                  Expanded(
                                    child: MaterialButton(
                                      onPressed: () async {
                                        print(_superAdminController.text);
                                        if (_fNameController.text.isEmpty ||
                                            _uNameController.text.isEmpty) {
                                          Fluttertoast.showToast(
                                              msg: "لا يمكن إضافة بإسم فارغ",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.BOTTOM,
                                              timeInSecForIosWeb: 1,
                                              backgroundColor: Colors.red,
                                              textColor: Colors.white,
                                              fontSize: 16.0);
                                        } else {
                                          print(_fNameController.text +
                                              _uNameController.text +
                                              _superAdminController.text +
                                              _passwordController.text +
                                              _emailController.text);
                                          _usersProvider.updateUserData(
                                              widget._userItemID.toString(),
                                              _fNameController.text,
                                              _uNameController.text,
                                              _superAdminController.text,
                                              _passwordController.text,
                                              _emailController.text);
                                        }
                                      },
                                      child: const Text(
                                        "تعديل العميل ",
                                        style: TextStyle(
                                          fontFamily: 'Jazeera',
                                          fontSize: 15,
                                          color: Colors.white,
                                        ),
                                      ),
                                      color: Colors.orange,
                                    ),
                                  ),
                                  const SizedBox(width: 3),
                                  Expanded(
                                    child: MaterialButton(
                                      onPressed: () {
                                        clearTextController();
                                      },
                                      child: const Text(
                                        "مسح البيانات",
                                        style: TextStyle(
                                          fontFamily: 'Jazeera',
                                          fontSize: 15,
                                          color: Colors.white,
                                        ),
                                      ),
                                      color: Colors.blueAccent,
                                    ),
                                  ),
                                  const SizedBox(width: 3),
                                  Expanded(
                                    child: MaterialButton(
                                      onPressed: () {
                                        showDialog<String>(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              AlertDialog(
                                            content: const Text(
                                                'هل تريد حذف العميل بالفعل ؟',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontFamily: 'Jazeera',
                                                    color: Colors.red)),
                                            actions: <Widget>[
                                              TextButton(
                                                onPressed: () => Navigator.pop(
                                                    context, 'Cancel'),
                                                child: const Text('إلغاء',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        fontFamily: 'Jazeera',
                                                        color: Colors.red)),
                                              ),
                                              TextButton(
                                                onPressed: () {
                                                  Navigator.of(context)
                                                      .pushReplacementNamed(
                                                          UsersPage.routeName);

                                                  _usersProvider.deleteUserByID(
                                                      widget._userItemID
                                                          .toString());
                                                },
                                                child: const Text('تأكيد',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: 15,
                                                        fontFamily: 'Jazeera',
                                                        color: Colors.red)),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                      child: const Text(
                                        "حذف العميل",
                                        style: TextStyle(
                                          fontFamily: 'Jazeera',
                                          fontSize: 15,
                                          color: Colors.white,
                                        ),
                                      ),
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                ),
              ]),
            ),
          ),
          if (Responsive.isDesktop(context))
            const Expanded(
              child: SideMenu(),
            ),
        ]),
      ),
    );
  }

  clearTextController() {
    _fNameController.clear();
    _emailController.clear();
    _uNameController.clear();
    _passwordController.clear();
    _superAdminController.clear();
  }
}
