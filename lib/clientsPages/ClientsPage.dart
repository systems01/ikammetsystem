import 'package:flutter/material.dart';
import 'package:ikkametsystem/components/sideMenu.dart';
import 'package:ikkametsystem/screens/homePage.dart';
import 'clientsListData.dart';

class ClientsPage extends StatefulWidget {
  static const routeName = '/ClientsPage';
  const ClientsPage({Key? key}) : super(key: key);

  @override
  _ClientsPageState createState() => _ClientsPageState();
}

class _ClientsPageState extends State<ClientsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'بيانات العملاء',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(HomePage.routeName);
            }),
      ),
      body: SafeArea(
        child:
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: const [
          Expanded(
            flex: 5,
            child: ClientsListData(),
          ),
          Expanded(
            child: SideMenu(),
          ),
        ]),
      ),
    );
  }
}
