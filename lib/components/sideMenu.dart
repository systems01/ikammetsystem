import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/clientsPages/ClientsPage.dart';
import 'package:ikkametsystem/logsPages/logsPage.dart';
import 'package:ikkametsystem/providers/logInProvider.dart';
import 'package:ikkametsystem/reportsPage/reportsPage.dart';
import 'package:ikkametsystem/screens/homePage.dart';
import '../screens/landingPage.dart';
import '../usersPages/usersPage.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LogInProvider newLoginProvider = LogInProvider();
    return Drawer(
      child: SingleChildScrollView(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(children: [
            DrawerHeader(
              child: SizedBox(
                  child: Image.network(
                      'https://c.tenor.com/4rjzFvNwsYwAAAAC/turkey-flag.gif'),
                  height: 50,
                  width: 50),
            ),
            DrawerListTile(
                title: 'لوحة التحكم',
                svSrc: Icons.dashboard_outlined,
                press: () {
                  Navigator.of(context)
                      .pushReplacementNamed(HomePage.routeName);
                }),
            DrawerListTile(
                title: 'المٌستخدمون',
                svSrc: Icons.supervised_user_circle_outlined,
                press: () {
                  Navigator.of(context)
                      .pushReplacementNamed(UsersPage.routeName);
                }),
            DrawerListTile(
                title: 'العملاء',
                svSrc: Icons.person,
                press: () {
                  Navigator.of(context)
                      .pushReplacementNamed(ClientsPage.routeName);
                }),
            DrawerListTile(
                title: 'التقارير',
                svSrc: Icons.report,
                press: () {
                  Navigator.of(context)
                      .pushReplacementNamed(ReportsPage.routeName);
                }),
            DrawerListTile(
                title: 'العمليات',
                svSrc: Icons.open_in_new_rounded,
                press: () {
                  Navigator.of(context)
                      .pushReplacementNamed(LogsPage.routeName);
                }),
            DrawerListTile(
                title: 'الحساب الشخصي',
                svSrc: Icons.person_pin_outlined,
                press: () {}),
            DrawerListTile(
                title: 'تسجيل الخروج',
                svSrc: Icons.logout,
                press: () {
                  logOut(context);
                }),
          ]),
        ),
      ),
    );
  }
}

Future logOut(BuildContext context) async {
  Navigator.of(context).pushReplacementNamed(LandingPage.routeName);
  Fluttertoast.showToast(
      msg: "تم تسجيل الخروج بنجاح شكراً لك ",
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0);
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    @required required this.title,
    @required required this.svSrc,
    required this.press,
  }) : super(key: key);

  final String title;
  final IconData svSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      hoverColor: Colors.black12,
      horizontalTitleGap: 0.0,
      leading: Icon(
        svSrc,
        color: Colors.red,
        size: 30,
      ),
      title: Text(
        title,
        textAlign: TextAlign.right,
        style: const TextStyle(
            fontSize: 15, fontFamily: 'Jazeera', color: Colors.red),
      ),
    );
  }
}
