import 'package:flutter/material.dart';
import 'package:ikkametsystem/components/sideMenu.dart';
import 'package:ikkametsystem/controller/menuController.dart';
import 'package:ikkametsystem/responsive/responsive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';

String name = 'sad';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      const ProfileCard(),
      SizedBox(
        width: Responsive.isDesktop(context) ? 10 : 5,
      ),
      const Expanded(
        child: SearchField(),
      ),
      Spacer(
        flex: Responsive.isDesktop(context) ? 2 : 1,
      ),
      const Text('لوحة التحكم',
          style: TextStyle(
              fontSize: 20, fontFamily: 'Jazeera', color: Colors.red)),
      if (!Responsive.isDesktop(context))
        IconButton(
            onPressed: context.read<MenuController>().controlMenu,
            icon: const Icon(
              Icons.menu,
              color: Colors.red,
            )),
    ]);
  }
}

class SearchField extends StatelessWidget {
  const SearchField({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextField(
      textAlign: TextAlign.right,
      decoration: InputDecoration(
        hintText: "بحث",
        hintStyle: const TextStyle(
          fontSize: 10,
          fontFamily: 'Jazeera',
        ),
        fillColor: Colors.black12,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        suffixIcon: InkWell(
          onTap: () {},
          child: Container(
            margin: const EdgeInsets.all(10 / 2),
            padding: const EdgeInsets.all(20 * 0.75),
            //   decoration: const BoxDecoration(color: Colors.blue),
            child: const Icon(Icons.search),
          ),
        ),
      ),
    );
  }
}

class ProfileCard extends StatefulWidget {
  const ProfileCard({Key? key}) : super(key: key);

  @override
  State<ProfileCard> createState() => _ProfileCardState();
}

class _ProfileCardState extends State<ProfileCard> {
  @override
  void setState(VoidCallback fn) {
    getUserNameValue();
    // TODO: implement setState
    super.setState(fn);
    getUserNameValue();
  }

  @override
  Widget build(BuildContext context) {
    getUserNameValue();
    print(name);
    return Container(
      margin: const EdgeInsets.only(right: 10),
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black12),
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Colors.black12),
      child: Row(children: [
        IconButton(
          icon: const Icon(Icons.arrow_drop_down),
          hoverColor: Colors.black12,
          onPressed: () {
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                content: const Text('هل تريد تسجيل الخروج بالفعل ؟',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20,
                        fontFamily: 'Jazeera',
                        color: Colors.red)),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: const Text('إلغاء',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Jazeera',
                            color: Colors.red)),
                  ),
                  TextButton(
                    onPressed: () => logOut(context),
                    child: const Text('تأكيد',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Jazeera',
                            color: Colors.red)),
                  ),
                ],
              ),
            );
          },
        ),
        Image.network(
          'https://c.tenor.com/4rjzFvNwsYwAAAAC/turkey-flag.gif',
          height: 38,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 1),
          child: Text(name,
              style: const TextStyle(
                  fontSize: 15, fontFamily: 'Jazeera', color: Colors.red)),
        ),
      ]),
    );
  }
}

getUserNameValue() async {
  final prefs = await SharedPreferences.getInstance();
  name = prefs.getString('firstname').toString();
  print(name);
  return name;
}
