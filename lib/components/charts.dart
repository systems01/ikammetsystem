import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

List<PieChartSectionData> paiCharSelectionData = [
  PieChartSectionData(
      value: 25, showTitle: false, radius: 15, color: Colors.blue),
  PieChartSectionData(
    value: 15,
    showTitle: false,
    radius: 15,
    color: const Color(0xFFEE2727),
  ),
  PieChartSectionData(
    value: 15,
    showTitle: false,
    radius: 15,
    color: const Color(0xFFFFCF26),
  ),
  PieChartSectionData(
    value: 15,
    showTitle: false,
    radius: 15,
    color: const Color(0xFF26E5FF),
  ),
];

class Charts extends StatelessWidget {
  const Charts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 180,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          PieChart(
            PieChartData(
              startDegreeOffset: -90,
              sectionsSpace: 0,
              centerSpaceRadius: 60,
              sections: paiCharSelectionData,
            ),
          ),
          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Text(
                  'العدد الكلي للعملاء',
                  style: TextStyle(
                      height: 0.5,
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Jazeera',
                      color: Colors.white),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '10',
                  style: TextStyle(
                      height: 0.5,
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Jazeera',
                      color: Colors.white),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
