import 'package:flutter/material.dart';
import 'package:ikkametsystem/logsPages/logsPage.dart';
import 'package:ikkametsystem/models/logItem.dart';
import 'package:ikkametsystem/providers/logsProvider.dart';

import '../components/sideMenu.dart';

LogsProvider _logsProvider = LogsProvider();
String imageCat = 'assets/images/search.png';

class LogDataDetials extends StatefulWidget {
  final LogItem _logItem;
  const LogDataDetials(this._logItem, {Key? key}) : super(key: key);

  @override
  State<LogDataDetials> createState() => _LogDataDetialsState();
}

class _LogDataDetialsState extends State<LogDataDetials> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            '${widget._logItem.logName}  تفاصيل عملية ',
            textAlign: TextAlign.right,
            style: const TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(LogsPage.routeName);
            }),
      ),
      body: SafeArea(
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
            flex: 5,
            child: SingleChildScrollView(
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Column(
                    children: [
                      Image.asset(
                        imageCat,
                        fit: BoxFit.fill,
                        height: 200,
                      ),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          ' إسم العملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'رقم العملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._logItem.logName}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._logItem.id}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'وصف العملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'تاريخ بدء العملية ',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._logItem.description}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._logItem.getCreatedAtDate()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'رقم الحساب الذي قام بالعملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'آخر تحديث للعملية العملية ',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._logItem.causerId}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._logItem.getUpdatedAtDate()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'مهام الحساب الذي قام بالعملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'وقت تحديث العملية ',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._logItem.causerType}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._logItem.getUpdatedAtTime()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'مهام الحساب الذي قامت عليه العملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'رقم الحساب الذي قامت عليه العملية',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._logItem.subjectType}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._logItem.subjectId}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(
                        children: [
                          FloatingActionButton.extended(
                            onPressed: () {
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  content: const Text(
                                      'هل تريد حذف العملية بالفعل ؟',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontFamily: 'Jazeera',
                                          color: Colors.red)),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Cancel'),
                                      child: const Text('إلغاء',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'Jazeera',
                                              color: Colors.red)),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushReplacementNamed(
                                                LogsPage.routeName);

                                        _logsProvider.deleteLogsByID(
                                            widget._logItem.id.toString());
                                      },
                                      child: const Text('تأكيد',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: 'Jazeera',
                                              color: Colors.red)),
                                    ),
                                  ],
                                ),
                              );
                            },
                            label: const Text(
                              "حذف العملية",
                              style: TextStyle(
                                fontFamily: 'Jazeera',
                                fontSize: 15,
                                color: Colors.white,
                              ),
                            ),
                            icon: const Icon(Icons.delete),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const Expanded(
            child: SideMenu(),
          ),
        ]),
      ),
    );
  }
}
