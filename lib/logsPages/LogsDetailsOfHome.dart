import 'package:flutter/material.dart';
import 'package:ikkametsystem/providers/logsProvider.dart';

import '../models/logItem.dart';
import 'logDataDetials.dart';

LogsProvider _logsProvider = LogsProvider();

class LogsDetails extends StatelessWidget {
  const LogsDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: const [
            Text(
              ' : آخر العمليات ',
              style: TextStyle(
                  fontSize: 20, fontFamily: 'Jazeera', color: Colors.red),
            ),
          ],
        ),
        const SizedBox(height: 10),
        SingleChildScrollView(
          controller: ScrollController(),
          child: Container(
            height: 500,
            decoration: const BoxDecoration(
              // color: Colors.blueGrey.shade300,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: FutureBuilder(
              future: _logsProvider.getAllLogs(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<LogItem>> snapshot) {
                if (snapshot.data == null) {
                  return const Center(
                    child: Text('من فضلك إنتظر '),
                  );
                } else {
                  return ListView.builder(
                    controller: ScrollController(),
                    itemCount: snapshot.data?.length,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    addAutomaticKeepAlives: false,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () async {
                          //
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  LogDataDetials(snapshot.data![index]),
                            ),
                          );
                        },
                        child: ClipRRect(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 4.0),
                            child: Card(
                              color: Colors.blueGrey.shade300,
                              margin: const EdgeInsets.only(
                                  left: 5.0,
                                  right: 5.0,
                                  bottom: 10.0,
                                  top: 0.0),
                              elevation: 4.0,
                              child: Column(
                                children: [
                                  Row(
                                    children: <Widget>[
                                      Stack(
                                        children: const <Widget>[
                                          Icon(
                                            Icons.open_in_new_rounded,
                                            size: 50,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      right: 8.0,
                                                    ),
                                                    child: Text(
                                                      ' ${snapshot.data![index].id.toString()}  :  رقم العملية  ',
                                                      style: const TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: 'Jazeera',
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign: TextAlign.end,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0),
                                                    child: Text(
                                                      '  ${snapshot.data![index].logName}  :  إسم العملية  ',
                                                      style: const TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: 'Jazeera',
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign: TextAlign.end,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0),
                                                    child: Text(
                                                      '${snapshot.data![index].getCreatedAtDate()}  : تاريخ العملية ',
                                                      style: const TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: 'Jazeera',
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign: TextAlign.end,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0),
                                                    child: Text(
                                                      '${snapshot.data![index].getCreatedAtTime()}  :  وقت العملية ',
                                                      style: const TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: 'Jazeera',
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign: TextAlign.end,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0),
                                                    child: Text(
                                                      '${snapshot.data![index].description.toString()}  : وصف العملية  ',
                                                      style: const TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: 'Jazeera',
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                      textAlign: TextAlign.end,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
