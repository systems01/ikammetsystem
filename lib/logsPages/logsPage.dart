import 'package:flutter/material.dart';
import 'package:ikkametsystem/logsPages/logListData.dart';

import '../components/sideMenu.dart';
import '../reportsPage/reportListData.dart';
import '../screens/homePage.dart';

class LogsPage extends StatefulWidget {
  static const routeName = '/LogsPage';
  const LogsPage({Key? key}) : super(key: key);

  @override
  State<LogsPage> createState() => _LogsPageState();
}

class _LogsPageState extends State<LogsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'جميع العمليات',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(HomePage.routeName);
            }),
      ),
      body: SafeArea(
        child:
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: const [
          Expanded(
            flex: 5,
            child: LogsListData(),
          ),
          Expanded(
            child: SideMenu(),
          ),
        ]),
      ),
    );
  }
}
