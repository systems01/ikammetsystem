import 'package:flutter/material.dart';
import 'package:ikkametsystem/models/reportItem.dart';
import 'package:ikkametsystem/reportsPage/reportsPage.dart';
import 'package:provider/provider.dart';
import '../components/sideMenu.dart';
import '../controller/menuController.dart';
import '../logsPages/logDataDetials.dart';
import '../responsive/responsive.dart';

class ReportDataDetails extends StatefulWidget {
  final ReportItem _reportItem;
  const ReportDataDetails(this._reportItem);

  @override
  State<ReportDataDetails> createState() => _ReportDataDetailsState();
}

class _ReportDataDetailsState extends State<ReportDataDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: context.read<MenuController>().scaffoldKey,
      drawer: const SideMenu(),
      appBar: AppBar(
        title: Center(
          child: Text(
            ' ${widget._reportItem.getReportName()} بيانات التقرير   ',
            textAlign: TextAlign.right,
            style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(ReportsPage.routeName);
            }),
      ),
      body: SafeArea(
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
            flex: 5,
            child: SingleChildScrollView(
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Column(
                    children: [
                      Image.asset(
                        imageCat,
                        fit: BoxFit.fill,
                        height: 300,
                      ),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          ' إسم التقرير',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'رقم التقرير',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._reportItem.getReportName()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._reportItem.id}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'وقت إصدار التقرير',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'تاريخ إصدار التقرير ',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._reportItem.getCreatedAtTime()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._reportItem.getCreatedAtDate()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'وقت  تحديث التقرير',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'آخر تحديث التقرير ',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                            child: Text(
                          '${widget._reportItem.getUpdatedAtTime()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                        const Spacer(),
                        Expanded(
                            child: Text(
                          '${widget._reportItem.getCreatedAtTime()}',
                          textAlign: TextAlign.right,
                          style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontFamily: 'Jazeera'),
                        )),
                      ]),
                      const SizedBox(height: 30),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const Expanded(
            child: SideMenu(),
          ),
        ]),
      ),
    );
  }
}
