import 'package:flutter/material.dart';
import 'package:ikkametsystem/models/reportItem.dart';
import 'package:ikkametsystem/providers/reportsProviders.dart';
import 'package:ikkametsystem/reportsPage/reportDataDetials.dart';

ReportsProvider _reportsProvider = ReportsProvider();

class ReportsListData extends StatefulWidget {
  const ReportsListData({Key? key}) : super(key: key);

  @override
  State<ReportsListData> createState() => _ReportsListDataState();
}

class _ReportsListDataState extends State<ReportsListData> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        controller: ScrollController(),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                height: MediaQuery.of(context).size.height - 80,
                child: FutureBuilder(
                  future: _reportsProvider.getAllReportsDataNew(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<ReportItem>> snapshot) {
                    // print(snapshot.data);
                    if (snapshot.data == null) {
                      return const Center(
                        child: Text('من فضلك إنتظر '),
                      );
                    } else {
                      return ListView.builder(
                        controller: ScrollController(),
                        itemCount: snapshot.data?.length,
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        addAutomaticKeepAlives: false,
                        itemBuilder: (BuildContext context, int index) {
                          print(index);
                          return InkWell(
                            onTap: () async {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      ReportDataDetails(snapshot.data![index]),
                                ),
                              );
                            },
                            child: ClipRRect(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: Card(
                                  color: Colors.teal,
                                  margin: const EdgeInsets.only(
                                      left: 5.0,
                                      right: 5.0,
                                      bottom: 10.0,
                                      top: 0.0),
                                  elevation: 4.0,
                                  child: Column(
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Stack(
                                            children: const <Widget>[
                                              Icon(
                                                Icons.attach_file_outlined,
                                                size: 50,
                                                color: Colors.white,
                                              ),
                                            ],
                                          ),
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(
                                                          right: 8.0,
                                                        ),
                                                        child: Text(
                                                          ' ${snapshot.data![index].getReportName()}  :  إسم التقرير  ',
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.white,
                                                            fontFamily:
                                                                'Jazeera',
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          textAlign:
                                                              TextAlign.end,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                right: 8.0),
                                                        child: Text(
                                                          '  ${snapshot.data![index].id.toString()}  :  رقم التقرير  ',
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.white,
                                                            fontFamily:
                                                                'Jazeera',
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          textAlign:
                                                              TextAlign.end,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                right: 8.0),
                                                        child: Text(
                                                          '${snapshot.data![index].getCreatedAtDate()}  :  تاريخ انشاء التقرير ',
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.white,
                                                            fontFamily:
                                                                'Jazeera',
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          textAlign:
                                                              TextAlign.end,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                right: 8.0),
                                                        child: Text(
                                                          '${snapshot.data![index].getUpdatedAtDate()}  :  تاريخ تعديل التقرير ',
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.white,
                                                            fontFamily:
                                                                'Jazeera',
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          textAlign:
                                                              TextAlign.end,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
