import 'package:flutter/material.dart';
import 'package:ikkametsystem/reportsPage/reportListData.dart';

import '../components/sideMenu.dart';
import '../screens/homePage.dart';
import '../usersPages/usersListData.dart';

class ReportsPage extends StatefulWidget {
  static const routeName = '/ReportsPage';
  const ReportsPage({Key? key}) : super(key: key);

  @override
  State<ReportsPage> createState() => _ReportsPageState();
}

class _ReportsPageState extends State<ReportsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'جميع التقارير',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(HomePage.routeName);
            }),
      ),
      body: SafeArea(
        child:
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: const [
          Expanded(
            flex: 5,
            child: ReportsListData(),
          ),
          Expanded(
            child: SideMenu(),
          ),
        ]),
      ),
    );
  }
}
