import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ikkametsystem/components/sideMenu.dart';
import 'package:ikkametsystem/providers/usersProvider.dart';
import 'package:ikkametsystem/usersPages/usersPage.dart';
import 'package:select_form_field/select_form_field.dart';

UsersProvider _usersProvider = UsersProvider();

class AddNewUser extends StatefulWidget {
  static const routeName = '/AddNewUser';
  const AddNewUser({Key? key}) : super(key: key);

  @override
  _AddNewUserState createState() => _AddNewUserState();
}

class _AddNewUserState extends State<AddNewUser> {
  final TextEditingController _fNameController = TextEditingController();
  final TextEditingController _uNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _superAdminController = TextEditingController();
  String imageCat = 'assets/images/12.png';

  final List<Map<String, dynamic>> _items = [
    {
      'value': '1',
      'label': 'أدمن',
      'icon': const Icon(Icons.stop),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
    {
      'value': '0',
      'label': 'مستخدم عادي',
      'icon': const Icon(Icons.fiber_manual_record),
      'textStyle': const TextStyle(fontSize: 15, fontFamily: 'Jazeera'),
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'إضافة مستخدم جديد',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(UsersPage.routeName);
            }),
      ),
      body: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Expanded(
          flex: 5,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Image.asset(
                        imageCat,
                        fit: BoxFit.fill,
                        height: 230,
                      ),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'الإسم',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'إسم المستخدم',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            controller: _fNameController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل الاسم',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            controller: _uNameController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل إسم المستخدم',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      const SizedBox(height: 20),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'كلمة المرور',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                        Spacer(),
                        Expanded(
                            child: Text(
                          'البريد',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: TextFormField(
                            controller: _passwordController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل كلمة المرو',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                        const Spacer(),
                        Expanded(
                          child: TextFormField(
                            controller: _emailController,
                            decoration: const InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: 'أدخل البريد الإلكتروني',
                              labelStyle: TextStyle(
                                  fontSize: 10, fontFamily: 'Jazeera'),
                            ),
                          ),
                        ),
                      ]),
                      const SizedBox(height: 20),
                      Row(children: const [
                        Expanded(
                            child: Text(
                          'نوع الحساب ',
                          textAlign: TextAlign.right,
                          style: TextStyle(fontSize: 18, fontFamily: 'Jazeera'),
                        )),
                      ]),
                      Row(children: [
                        Expanded(
                          child: SelectFormField(
                            controller: _superAdminController,
                            type: SelectFormFieldType
                                .dropdown, // or can be dialog
                            //initialValue: '1',
                            icon: const Icon(Icons.format_shapes),
                            labelText: 'إختر نوع الحساب',
                            style: const TextStyle(
                                fontSize: 10, fontFamily: 'Jazeera'),
                            items: _items,
                            onChanged: (val) => print(val),
                            onSaved: (val) => _superAdminController.text = val!,
                          ),
                        )
                      ]),
                      const SizedBox(height: 30),
                      Row(
                        children: [
                          Expanded(
                            child: MaterialButton(
                              onPressed: () async {
                                print(_superAdminController.text);
                                if (_fNameController.text.isEmpty ||
                                    _uNameController.text.isEmpty) {
                                  Fluttertoast.showToast(
                                      msg: "لا يمكن إضافة بإسم فارغ",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.red,
                                      textColor: Colors.white,
                                      fontSize: 16.0);
                                } else {
                                  print(_fNameController.text +
                                      _uNameController.text +
                                      _superAdminController.text +
                                      _passwordController.text +
                                      _emailController.text);
                                  _usersProvider.addNewUser(
                                      _fNameController.text,
                                      _uNameController.text,
                                      _superAdminController.text,
                                      _passwordController.text,
                                      _emailController.text);
                                }
                              },
                              child: const Text(
                                "إضافة مستخدم ",
                                style: TextStyle(
                                  fontFamily: 'Jazeera',
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                              color: Colors.green,
                            ),
                          ),
                          const SizedBox(width: 3),
                          Expanded(
                            child: MaterialButton(
                              onPressed: () {
                                clearTextController();
                              },
                              child: const Text(
                                "مسح البيانات",
                                style: TextStyle(
                                  fontFamily: 'Jazeera',
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                              color: Colors.blueAccent,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        const Expanded(
          child: SideMenu(),
        ),
      ]),
    );
  }

  clearTextController() {
    _fNameController.clear();
    _emailController.clear();
    _uNameController.clear();
    _passwordController.clear();
    _superAdminController.clear();
  }
}
