import 'package:flutter/material.dart';
import 'package:ikkametsystem/providers/clientsProvider.dart';
import 'package:ikkametsystem/providers/usersProvider.dart';
import 'package:ikkametsystem/usersPages/storageUsersInfo.dart';

import '../components/charts.dart';

UsersProvider _usersProvider = UsersProvider();
ClientsProvider _clientsProvider = ClientsProvider();
var clientsCount = 0;
var userCount = 0;

class StorageUsersInfoDetails extends StatefulWidget {
  const StorageUsersInfoDetails({Key? key}) : super(key: key);

  @override
  State<StorageUsersInfoDetails> createState() =>
      _StorageUsersInfoDetailsState();
}

class _StorageUsersInfoDetailsState extends State<StorageUsersInfoDetails> {
  userCountRes() async {
    userCount = await _usersProvider.getUsersCount();
  }

  clientsCountRes() async {
    userCount = await _usersProvider.getUsersCount();
  }

  @override
  void setState(VoidCallback fn) {
    // TODO: implement setState
    userCountRes();
    super.setState(fn);
    userCountRes();
  }

  @override
  void initState() {
    // TODO: implement initState
    userCountRes();
    super.initState();
    userCountRes();
  }

  @override
  Widget build(BuildContext context) {
    userCountRes();
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.blueGrey.shade300,
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "تحليل البيانات العام",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                fontFamily: 'Jazeera',
                color: Colors.white),
          ),
          const Charts(),
          StorageUsersInfo(
            icon: Icons.supervised_user_circle_outlined,
            numberOfUs: userCount,
            textOfTes: 'الحالي',
            title: 'المُستخدمون',
            color: Colors.blue,
          ),
          const StorageUsersInfo(
            icon: Icons.person,
            numberOfUs: 100,
            textOfTes: 'الحالي',
            title: 'العملاء',
            color: const Color(0xFFEE2727),
          ),
          const StorageUsersInfo(
            icon: Icons.report,
            numberOfUs: 5,
            textOfTes: 'الحالي',
            title: 'التقارير',
            color: const Color(0xFFFFCF26),
          ),
          const StorageUsersInfo(
            icon: Icons.open_in_new_rounded,
            numberOfUs: 90,
            textOfTes: 'الحالي',
            title: 'العمليات',
            color: const Color(0xFF26E5FF),
          ),
        ],
      ),
    );
  }
}
