// ignore_for_file: use_key_in_widget_constructors

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ikkametsystem/providers/usersProvider.dart';
import 'package:ikkametsystem/search/userSearch.dart';
import 'package:ikkametsystem/usersPages/userDataDetails.dart';
import 'package:pagination_view/pagination_view.dart';

PaginationViewType paginationViewType = PaginationViewType.listView;
GlobalKey<PaginationViewState> key = GlobalKey<PaginationViewState>();
String imageUser = 'https://c.tenor.com/4rjzFvNwsYwAAAAC/turkey-flag.gif';
int page = 0;
UsersProvider _usersProvider = UsersProvider();
TextEditingController searchController = TextEditingController();

class UserListData extends StatefulWidget {
  const UserListData({Key? key}) : super(key: key);

  @override
  State<UserListData> createState() => _UserListDataState();
}

class _UserListDataState extends State<UserListData> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: ScrollController(),
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Row(children: [
              const Expanded(
                flex: 3,
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: SearchUsersField(),
                ),
              ),
              usersOptions(context),
            ]),
            Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              height: MediaQuery.of(context).size.height - 80,
              child: FutureBuilder(
                future: _usersProvider.getAllUsersDataHttp(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (kDebugMode) {
                    print(snapshot.data);
                  }
                  if (snapshot.data == null ||
                      snapshot.data == ConnectionState.none ||
                      snapshot.data == ConnectionState.waiting) {
                    return const Center(
                      child: Text('من فضلك إنتظر '),
                    );
                  } else {
                    return ListView.builder(
                      controller: ScrollController(),
                      itemCount: snapshot.data['users'].length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      addAutomaticKeepAlives: false,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () async {
                            // Navigator.of(context)
                            //     .pushReplacementNamed(UserDataDetails.routName);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => UserDataDetails(snapshot
                                    .data!['users'][index]['id']
                                    .toString()),
                              ),
                            );
                          },
                          child: ClipRRect(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Card(
                                color: Colors.redAccent,
                                margin: const EdgeInsets.only(
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 10.0,
                                    top: 0.0),
                                elevation: 4.0,
                                child: Column(
                                  children: [
                                    Row(
                                      children: <Widget>[
                                        Stack(
                                          children: const <Widget>[
                                            Icon(
                                              Icons.person_pin_outlined,
                                              size: 50,
                                              color: Colors.white,
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        right: 8.0,
                                                      ),
                                                      child: Text(
                                                        ' ${snapshot.data!['users'][index]['id'].toString()}  :  رقم المستخدم  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data["users"][index]["firstName"]}  :  إسم المستخدم الأول  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data["users"][index]["email"]}  :  بريد المستخدم  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              right: 8.0),
                                                      child: Text(
                                                        ' ${snapshot.data["users"][index]["username"]}  :  إسم المستخدم  ',
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: 'Jazeera',
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.end,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget usersOptions(BuildContext context) {
  return Container(
    color: Colors.transparent,
    padding: const EdgeInsets.all(10),
    child: Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(width: 10),
          FloatingActionButton.extended(
            heroTag: "6",
            backgroundColor: Colors.green,
            hoverColor: Colors.transparent,
            onPressed: (() {
              Navigator.of(context).pushReplacementNamed("/AddNewUser");
            }),
            label: const Text(
              'أضف مستخدم جديد',
              textAlign: TextAlign.right,
              style: TextStyle(
                  fontSize: 15, fontFamily: 'Jazeera', color: Colors.white),
            ),
            icon: const Icon(Icons.add),
          ),
        ],
      ),
    ]),
  );
}

class SearchUsersField extends StatelessWidget {
  const SearchUsersField();
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: searchController,
      textAlign: TextAlign.right,
      decoration: InputDecoration(
        hintText: " بحث في المستخدمين",
        hintStyle: const TextStyle(
          fontSize: 10,
          fontFamily: 'Jazeera',
        ),
        fillColor: Colors.black12,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        suffixIcon: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => UserSearch(searchController.text),
              ),
            );
          },
          child: Container(
            margin: const EdgeInsets.all(10 / 2),
            padding: const EdgeInsets.all(20 * 0.75),
            //   decoration: const BoxDecoration(color: Colors.blue),
            child: const Icon(Icons.search),
          ),
        ),
      ),
    );
  }
}
