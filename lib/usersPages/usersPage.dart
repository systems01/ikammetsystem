import 'package:flutter/material.dart';
import 'package:ikkametsystem/components/sideMenu.dart';
import 'package:ikkametsystem/screens/homePage.dart';
import 'package:ikkametsystem/usersPages/usersListData.dart';

class UsersPage extends StatefulWidget {
  static const routeName = '/UsersPage';
  const UsersPage({Key? key}) : super(key: key);

  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'بيانات المستخدمون',
            textAlign: TextAlign.right,
            style: TextStyle(fontSize: 20, fontFamily: 'Jazeera'),
          ),
        ),
        leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(HomePage.routeName);
            }),
      ),
      body: SafeArea(
        child:
            Row(crossAxisAlignment: CrossAxisAlignment.start, children: const [
          Expanded(
            flex: 5,
            child: UserListData(),
          ),
          Expanded(
            child: SideMenu(),
          ),
        ]),
      ),
    );
  }
}
