import 'package:flutter/material.dart';

class StorageUsersInfo extends StatelessWidget {
  const StorageUsersInfo(
      {Key? key,
      required this.title,
      required this.numberOfUs,
      required this.textOfTes,
      required this.icon,
      required this.color})
      : super(key: key);

  final String title, textOfTes;
  final IconData icon;
  final int numberOfUs;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: color,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Row(
        children: [
          Text(
            textOfTes,
            style: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w500,
                fontFamily: 'Jazeera',
                color: color),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Jazeera',
                        color: color),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    '$numberOfUs ',
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Jazeera',
                        color: color),
                  ),
                ],
              ),
            ),
          ),

          SizedBox(
            width: 20,
            height: 20,
            child: Icon(
              icon,
              color: color,
            ),
          ),

          //
        ],
      ),
    );
  }
}
