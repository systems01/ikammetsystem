import 'package:flutter/material.dart';
import 'package:ikkametsystem/components/header.dart';
import 'package:ikkametsystem/responsive/responsive.dart';
import '../clientsPages/clientsNeedUpdatesList.dart';
import '../logsPages/LogsDetailsOfHome.dart';
import '../usersPages/storageUsersInfoDetails.dart';

class DashBoardPage extends StatefulWidget {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          controller: ScrollController(),
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              const Header(),
              const SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (!Responsive.isMobile(context))
                    const Expanded(
                      flex: 1,
                      child: StorageUsersInfoDetails(),
                    ),
                  if (Responsive.isMobile(context)) const SizedBox(width: 5),
                  Expanded(
                    flex: 5,
                    child: Column(
                      children: const [
                        LogsDetails(),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text(
                    '  :آخر التحديثات للعملاء - عملاء تحتاج للمراجعة',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 20, fontFamily: 'Jazeera', color: Colors.red),
                  ),
                  const SizedBox(height: 5),
                  const ClientsNeedUpdateList(),
                  if (Responsive.isMobile(context)) const SizedBox(height: 10),
                  if (Responsive.isMobile(context))
                    const StorageUsersInfoDetails(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
