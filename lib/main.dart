import 'package:flutter/material.dart';
import 'package:ikkametsystem/clientsPages/addNewClient.dart';
import 'package:ikkametsystem/controller/menuController.dart';
import 'package:ikkametsystem/logsPages/logsPage.dart';
import 'package:ikkametsystem/models/userItem.dart';
import 'package:ikkametsystem/reportsPage/reportsPage.dart';
import 'package:ikkametsystem/usersPages/addNewUser.dart';
import 'package:ikkametsystem/clientsPages/ClientsPage.dart';
import 'package:ikkametsystem/screens/homePage.dart';
import 'package:ikkametsystem/screens/landingPage.dart';
import 'package:ikkametsystem/screens/logInPage.dart';
import 'package:ikkametsystem/screens/reSetPassword.dart';
import 'package:ikkametsystem/screens/signUpPage.dart';
import 'package:ikkametsystem/usersPages/userDataDetails.dart';
import 'package:ikkametsystem/usersPages/usersPage.dart';
import 'package:provider/provider.dart';

late String _userItem;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(ChangeNotifierProvider<MenuController>(
    create: (_) => MenuController(),
    child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'نظام الإقامة التركية',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => MenuController(),
          ),
        ],
        child: const LandingPage(),
      ),
      routes: {
        LandingPage.routeName: (ctx) => const LandingPage(),
        SignUpPage.routeName: (ctx) => const SignUpPage(),
        LoginPage.routeName: (ctx) => const LoginPage(),
        ReSetPassword.routeName: (ctx) => const ReSetPassword(),
        HomePage.routeName: (ctx) => const HomePage(),
        UsersPage.routeName: (ctx) => const UsersPage(),
        ClientsPage.routeName: (ctx) => const ClientsPage(),
        ReportsPage.routeName: (ctx) => const ReportsPage(),
        LogsPage.routeName: (ctx) => const LogsPage(),
        AddNewClient.routeName: (ctx) => const AddNewClient(),
        AddNewUser.routeName: (ctx) => const AddNewUser(),
        UserDataDetails.routName: (ctx) => UserDataDetails(_userItem),
      },
    ),
  ));
}
